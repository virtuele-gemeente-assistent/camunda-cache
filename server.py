import copy
import logging
import os
from datetime import datetime

import httpx
from aiocache import Cache
from elasticapm.contrib.starlette import ElasticAPM, make_apm_client
from fastapi import FastAPI, HTTPException, Request
from fastapi.responses import Response

from camunda_cache.camunda import Camunda
from camunda_cache.config import Config
from camunda_cache.dmn_watcher import DMNWatcher
from camunda_cache.processing_tracker import ProcessingTracker

# Configure logging
logger = logging.getLogger("uvicorn.error")
logger.setLevel(logging.INFO)

# Load environment variables
cms_base_url = Config.CMS_BASE_URL
media_container_url = Config.MEDIA_CONTAINER_URL
camunda_url = Config.CAMUNDA_URL
sentry_dsn = Config.SENTRY_DSN

# Initialize global variables
loaded_dmn_files = {"files": [], "etag": None}
cms_caching = {}

# Configure Sentry for error monitoring
if sentry_dsn:
    import sentry_sdk

    sentry_sdk.init(dsn=sentry_dsn)

# Initialize FastAPI app
app = FastAPI()

if os.getenv("ELASTIC_APM_SERVER_URL"):
    environment = os.getenv("ENVIRONMENT")
    apm = make_apm_client(
        {
            "ELASTIC_APM_SERVER_URL": os.getenv("ELASTIC_APM_SERVER_URL"),
            "ELASTIC_APM_SECRET_TOKEN": os.getenv("ELASTIC_APM_SECRET_TOKEN"),
            "SERVICE_NAME": f"Gem Camunda Cache {environment}",
        }
    )
    app.add_middleware(ElasticAPM, client=apm)


@app.on_event("startup")
async def startup():
    try:
        # Initialize state
        app.state.variables = {}
        app.state.last_activity = {}  # Track last activity per tenant
        app.state.camunda = Camunda(httpx.AsyncClient(timeout=None))
        app.state.processing_tracker = ProcessingTracker()
        app.state.dmn_watcher = DMNWatcher(
            app.state.camunda,
            app.state.processing_tracker,
            app.state.last_activity,
            app.state.variables,
        )

        # Track the currently running processing tasks
        app.state.active_processing_tasks = {}

        # Wait for Camunda to fully initialize
        logger.info("Waiting for Camunda to start up...")
        await app.state.camunda.wait_for_startup(str(camunda_url / "engine"))

        # Wait for the media container to fully initialize
        logger.info("Waiting for media container to start up...")
        await app.state.camunda.wait_for_startup(media_container_url)

        # Start watchers for each environment
        logger.info("Starting watchers for tenants...")
        await app.state.dmn_watcher.start_listeners_for_tenants()

    except Exception as e:
        logger.exception("Error during application startup: %s", e)
        raise


@app.on_event("shutdown")
async def shutdown():
    # Cleanup the HTTP client and clear the Camunda namespace cache
    await app.state.camunda.close()
    await Cache(Cache.REDIS, endpoint="redis", port="6379").clear(namespace="camunda")


@app.get("/health")
async def health():
    return {"status": "ok"}


@app.get("/get_camunda_deployments_names")
async def get_camunda_deployments_names():
    return await app.state.camunda.get_deployment_names_from_camunda()


@app.get("/status/{etag}")
async def get_dmn_processing_status(etag: str):
    """
    Get the processing status for a specific ETag.
    Returns one of:
      - "done": DMN files have been successfully processed
      - "inprogress": DMN files are still being processed
      - "error": An error occurred during processing
    """
    status = app.state.processing_tracker.get_status(etag)
    return {"status": status, "etag": etag}


@app.get("/dmn/list")
async def get_dmn_files(tenant: str = "gem-media-container"):
    """List DMN files for the specified tenant."""
    return app.state.camunda.dmn_filenames(tenant=tenant)


@app.get("/dmn/{filename}")
async def get_dmn(filename: str, tenant: str = "gem-media-container"):
    """Fetch a DMN file by filename."""
    dmn_file_content = await app.state.camunda.get_dmn(filename, tenant=tenant)
    return Response(content=dmn_file_content, media_type="application/xml")


@app.get("/variables")
async def get_variables(project_name: str = "gem-media-container") -> list[str]:
    """Retrieve the current variables for a specific tenant."""
    tenant = await app.state.camunda.get_project_name_from_tenant_mapping(project_name)
    if tenant in app.state.variables:
        return list(app.state.variables[tenant].keys())
    else:
        raise HTTPException(status_code=404, detail="No variables loaded for tenant.")


@app.post("/evaluate/{project_name}")
async def camunda_evaluate(req: Request, project_name: str = "gem-media-container"):
    """Evaluate Camunda definitions based on the provided payload."""
    payload = await req.json()

    tenant = await app.state.camunda.get_project_name_from_tenant_mapping(project_name)
    if not tenant:
        tenant = project_name  # fallback if mapping not found

    # Update last activity for the tenant
    app.state.last_activity[tenant] = datetime.now()

    # Load existing variables (deep-copied for safety)
    existing_variables = app.state.variables.get(tenant, {})
    variables = copy.deepcopy(existing_variables)

    # Merge incoming variables into our local store
    incoming_variables = payload.get("variables", {})
    variables.update(incoming_variables)
    payload["variables"] = variables

    logger.info("Evaluating Camunda definitions for tenant: %s", tenant)

    # Perform DMN evaluation
    (
        response_list,
        name_list,
        status_code,
        status_text,
    ) = await app.state.camunda.evaluate(
        cms_base_url, cms_caching, payload, tenant=tenant
    )

    return {
        "responses": response_list,
        "names": name_list,
        "status_code": status_code,
        "status_text": status_text,
    }


if __name__ == "__main__":
    import uvicorn

    uvicorn.run(app, host="localhost", port=8000)

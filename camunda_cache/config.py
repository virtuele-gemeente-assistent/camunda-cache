import os
from yarl import URL


class Config:
    CMS_BASE_URL = os.getenv("ANTWOORD_CMS_URL")
    MEDIA_CONTAINER_URL = os.getenv("MEDIA_CONTAINER_URL", "http://media:80")
    CAMUNDA_URL = URL(os.getenv("CAMUNDA_ENGINE_URL"))
    SENTRY_DSN = os.getenv("CAMUNDA_CACHE_SENTRY_DSN")
    ELASTIC_APM_SERVER_URL = os.getenv("ELASTIC_APM_SERVER_URL")
    ELASTIC_APM_SECRET_TOKEN = os.getenv("ELASTIC_APM_SECRET_TOKEN")
    ENVIRONMENT = os.getenv("ENVIRONMENT", "production")
    REDIS_ENDPOINT = os.getenv("REDIS_ENDPOINT", "redis")
    REDIS_PORT = os.getenv("REDIS_PORT", "6379")

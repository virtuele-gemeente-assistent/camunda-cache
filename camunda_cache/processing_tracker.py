from enum import Enum
from typing import Dict


class ProcessingStatus(str, Enum):
    """Processing status of the request"""

    OK = "done"
    IN_PROGRESS = "inprogress"
    ERROR = "error"
    NOT_FOUND = "notfound"


# Add this to the FastAPI app initialization section
class ProcessingTracker:
    """Keeps track of the processing status of the requests"""

    def __init__(self):
        self.status: Dict[str, ProcessingStatus] = {}

    def start_processing(self, etag: str):
        self.status[etag] = ProcessingStatus.IN_PROGRESS

    def complete_processing(self, etag: str):
        self.status[etag] = ProcessingStatus.OK

    def mark_error(self, etag: str):
        self.status[etag] = ProcessingStatus.ERROR

    def get_status(self, etag: str) -> ProcessingStatus:
        return self.status.get(etag, ProcessingStatus.NOT_FOUND)

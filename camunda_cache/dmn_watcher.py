# app/services/dmn_watcher.py
import asyncio
import logging
from typing import Dict
import httpx

from camunda_cache.processing_tracker import ProcessingTracker
from camunda_cache.camunda import Camunda
from camunda_cache.config import Config

logger = logging.getLogger("uvicorn.error")


class DMNWatcher:
    def __init__(
        self,
        camunda_service: Camunda,
        processing_tracker: ProcessingTracker,
        last_activity: dict,
        variables: dict,
    ):
        self.camunda_service = camunda_service
        self.processing_tracker = processing_tracker
        self.last_activity = last_activity
        self.variables = variables
        self.active_processing_tasks: Dict[str, asyncio.Task] = {}

    async def get_config_from_media_container(self, root: str = "gem") -> dict:
        async with httpx.AsyncClient() as client:
            response = await client.get(f"{Config.MEDIA_CONTAINER_URL}/{root}")
            response.raise_for_status()
            return response.json()

    async def process_dmn_updates(
        self, etag: str, environment: str, max_retries: int = 3, delay: int = 10
    ):
        attempt = 0
        while attempt < max_retries:
            try:
                logger.info(
                    "Starting DMN processing task for environment '%s' with ETag '%s' (Attempt %d/%d)",
                    environment,
                    etag,
                    attempt + 1,
                    max_retries,
                )

                # Mark the ETag as processing
                self.processing_tracker.start_processing(etag)

                # Perform the necessary updates
                await self.camunda_service.clean_up_old_tenants(self.last_activity)

                tenant = etag
                version = etag

                # Update project -> tenant mapping
                await self.camunda_service.update_project_name_to_tenant_mapping(
                    version, tenant
                )

                # Clear the cache, load DMNs, and refresh variables for this tenant
                await self.camunda_service.clear_cache(namespace=tenant)
                await self.camunda_service.load_camunda(tenant=tenant)
                self.variables[tenant] = await self.camunda_service.get_variables(
                    tenant=tenant
                )

                logger.info(
                    "Successfully processed DMN updates for tenant '%s'.", tenant
                )
                self.processing_tracker.complete_processing(etag)
                return  # Exit after successful processing

            except asyncio.CancelledError:
                logger.warning(
                    "Processing task for ETag '%s' in environment '%s' was canceled.",
                    etag,
                    environment,
                )
                raise  # Allow task cancellation

            except Exception as e:
                attempt += 1
                logger.error(
                    "Error processing DMN updates for environment '%s', ETag '%s': %s",
                    environment,
                    etag,
                    e,
                )
                if attempt < max_retries:
                    logger.info(
                        "Retrying DMN processing for environment '%s' in %d seconds (Attempt %d/%d).",
                        environment,
                        delay,
                        attempt + 1,
                        max_retries,
                    )
                    await asyncio.sleep(delay)
                else:
                    logger.error(
                        "Maximum retry attempts reached for environment '%s', ETag '%s'. Marking as ERROR.",
                        environment,
                        etag,
                    )
                    self.processing_tracker.mark_error(etag)
                    return  # Exit after reaching max retries

    async def watch_dmn_updates_for_environment(
        self,
        root: str,
        environment: str,
        initial_delay: int = 30,
        max_retries: int = 5,
    ):
        while True:
            try:
                logger.info(
                    "Checking for DMN updates in environment '%s'.", environment
                )

                if not Config.MEDIA_CONTAINER_URL or not environment:
                    raise ValueError(
                        "Media container URL or environment is not defined."
                    )

                # Check for updates using the method in Camunda
                has_updates, etag = await self.camunda_service.check_for_updates(
                    root, Config.MEDIA_CONTAINER_URL, environment
                )
                logger.info(
                    "Watcher - has_updates=%s, environment=%s", has_updates, environment
                )

                if has_updates and etag:
                    logger.info(
                        "Detected new DMN updates for environment '%s' with ETag '%s'.",
                        environment,
                        etag,
                    )

                    # Cancel any existing processing task for this environment
                    existing_task = self.active_processing_tasks.get(environment)
                    if existing_task and not existing_task.done():
                        logger.info(
                            "Cancelling existing processing task for environment '%s'.",
                            environment,
                        )
                        existing_task.cancel()
                        try:
                            await existing_task
                        except asyncio.CancelledError:
                            logger.info(
                                "Ongoing task for '%s' was canceled successfully.",
                                environment,
                            )

                    # Create a new processing task for the new ETag
                    new_task = asyncio.create_task(
                        self.process_dmn_updates(etag, environment, max_retries)
                    )
                    self.active_processing_tasks[environment] = new_task

                else:
                    logger.info(
                        "No updates detected for environment '%s'. "
                        "Sleeping for %d seconds.",
                        environment,
                        initial_delay,
                    )

            except Exception as error:
                logger.warning(
                    "Error checking for DMN updates in environment '%s': %s. "
                    "Sleeping for %d seconds before retrying.",
                    environment,
                    error,
                    initial_delay,
                )

            # Sleep before the next iteration
            await asyncio.sleep(initial_delay)

    async def start_listeners_for_tenants(self, root: str = "gem"):
        try:
            payload = await self.get_config_from_media_container(root=root)
            paths = payload.get("paths")

            if not paths or not isinstance(paths, dict):
                logger.error("Invalid or missing 'paths' in media container payload.")
                raise ValueError(
                    "No valid 'paths' found in the media container payload."
                )

            environments = list(paths.keys())
            logger.info("Environments detected: %s", environments)

            # Launch a watcher for each environment
            for environment in environments:
                asyncio.create_task(
                    self.watch_dmn_updates_for_environment(
                        root, environment, initial_delay=30, max_retries=5
                    )
                )
        except Exception as e:
            logger.exception("Error while starting tenant listeners: %s", e)
            raise

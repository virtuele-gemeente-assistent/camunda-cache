import asyncio
import hashlib
import json
import logging
import os
import shutil
import urllib.parse
import xml.etree.ElementTree as ET
from datetime import datetime
from typing import Dict, Optional, Set, Tuple

import aiofiles
import aiocache
from aiocache import cached
from aiocache.serializers import JsonSerializer
import httpx
from pydantic import BaseModel
from slugify import slugify
from yarl import URL

logger = logging.getLogger("uvicorn.error")
logger.setLevel(logging.INFO)

CAMUNDA_URL = URL(os.getenv("CAMUNDA_ENGINE_URL"))
CAMUNDA_DATA_DIR = os.path.join(os.getcwd(), "data/camunda-dmn")


def _key_builder(func, *args, **kwargs):
    if "definition_key" not in kwargs or "payload" not in kwargs:
        raise ValueError("definition_key and payload must be defined!")

    # Ensure payload is sorted consistently to avoid key mismatches
    sorted_payload = dict(sorted(kwargs["payload"].items()))
    key = f"{kwargs['definition_key']}:{json.dumps(sorted_payload, sort_keys=True)}:{kwargs.get('tenant', 'camunda')}"
    
    # Log the generated key
    hashed_key = hashlib.md5(key.encode()).hexdigest()
    logger.debug(
        "Generated cache key: %s for definition_key=%s",
        hashed_key,
        kwargs["definition_key"],
    )

    return hashed_key


class Definition(BaseModel):
    version: int
    name: str


class Camunda:
    MAX_TENANTS = 5

    def __init__(self, client: httpx.AsyncClient = httpx.AsyncClient()):
        self.client = client
        self.definitions = {}
        self.etags = {}
        self.cache = aiocache.Cache(aiocache.RedisCache, endpoint="redis", port="6379")
        # Remove all existing folders in the data directory, even if they are not empty
        self._initialize_data_directory()

    def _initialize_data_directory(self):
        """Deletes all contents of the data directory and recreates it."""
        if os.path.exists(CAMUNDA_DATA_DIR):
            self._clear_directory(CAMUNDA_DATA_DIR)

        # Create the directory if it doesn't exist
        os.makedirs(CAMUNDA_DATA_DIR, exist_ok=True)

    def _clear_directory(self, directory_path: str):
        """Removes all folders and files in the specified directory."""
        for entry in os.listdir(directory_path):
            entry_path = os.path.join(directory_path, entry)
            if os.path.isdir(entry_path):
                shutil.rmtree(entry_path)
            elif os.path.isfile(entry_path):
                os.remove(entry_path)

    async def clean_up_old_tenants(self, last_activity_dict, keep_top_n=5):
        """
        Remove tenants beyond the most recent `keep_top_n` based on last activity.
        """
        # Sort tenants by last activity, most recent first
        sorted_tenants = sorted(
            last_activity_dict.items(), key=lambda item: item[1], reverse=True
        )

        if len(sorted_tenants) > keep_top_n:
            # Determine tenants to remove
            to_remove = sorted_tenants[keep_top_n:]

            for tenant, _ in to_remove:
                await self.delete_tenant(tenant)
                self._remove_local_tenant_data(tenant)

                # Remove from last_activity tracking
                last_activity_dict.pop(tenant, None)

            logger.info("Cleaned up old tenants: %s", to_remove)

    def _remove_local_tenant_data(self, tenant):
        """Removes local data related to tenant."""
        tenant_dir = os.path.join(CAMUNDA_DATA_DIR, tenant)
        if os.path.exists(tenant_dir):
            shutil.rmtree(tenant_dir)
            logger.info("Removed local data for tenant %s.", tenant)

    async def close(self):
        """
        Close the HTTP client.
        """
        await self.client.aclose()

    async def dmn_filenames(self, tenant: str = "gem-media-container"):
        """
        Return a list of DMN filenames for the specified tenant asynchronously.
        """
        tenant_dir = os.path.join(CAMUNDA_DATA_DIR, tenant)

        # Ensure the tenant directory exists using asyncio.to_thread to call os.makedirs in a separate thread
        await asyncio.to_thread(os.makedirs, tenant_dir, exist_ok=True)

        # List DMN files in the tenant directory asynchronously using asyncio.to_thread for os.listdir
        dmn_files = await asyncio.to_thread(
            lambda: [f for f in os.listdir(tenant_dir) if f.endswith(".dmn")]
        )

        return dmn_files

    async def wait_for_startup(self, url, num_retries=5, timeout=10):
        """
        Wait for the Camunda engine to start up.
        """
        for _ in range(num_retries):
            try:
                response = await self.client.get(url)
                if response.status_code == 200:
                    break
            except httpx.RequestError:
                await asyncio.sleep(timeout)

    async def load_camunda(self, tenant: str):
        """
        Load DMN files into Camunda for the specified tenant.
        """
        url = str(CAMUNDA_URL / "deployment/create")

        # Await the dmn_filenames call
        dmn_files = await self.dmn_filenames(tenant=tenant)  # Fix: await this call

        files = {}
        for f in dmn_files:
            # Construct the file path
            file_path = os.path.join(CAMUNDA_DATA_DIR, tenant, f)

            # Open the file asynchronously and read content
            async with aiofiles.open(file_path, "rb") as file:
                content = await file.read()
                files[f] = (f, content, "application/octet-stream")

        if files:
            response = await self.client.post(
                url=url,
                files={
                    "deployment-name": (None, tenant.encode("UTF-8"), "text/plain"),
                    "enable-duplicate-filtering": (
                        None,
                        "true".encode("UTF-8"),
                        "text/plain",
                    ),
                    "tenant-id": (None, tenant.encode("UTF-8"), "text/plain"),
                    **files,
                },
            )

            if response.status_code == 200:
                logger.info("DMN files loaded successfully for tenant %s.", tenant)
            else:
                logger.error(
                    "Failed to load DMN files for tenant %s, status code: %s",
                    tenant,
                    response.status_code,
                )

        else:
            logger.info("No DMN files found for tenant %s.", tenant)

    async def get_deployment_names_from_camunda(self):
        """
        Fetch deployment names from Camunda.
        """
        url = str(CAMUNDA_URL / "deployment")
        try:
            response = await self.client.get(url=url)

            # Check if the response is successful
            if response.status_code == 200:
                try:
                    deployments = response.json()
                    logger.info("Retrieved deployments: %s", deployments)
                    return deployments
                except json.JSONDecodeError as e:
                    logger.error("Failed to parse JSON from Camunda: %s", e)
                    return {"error": "Failed to parse JSON"}
            else:
                logger.error(
                    "Error fetching deployments, status code: %s", response.status_code
                )
                return {"error": f"Request failed with status {response.status_code}"}

        except httpx.RequestError as e:
            logger.error("Request error occurred: %s", e)
            return {"error": "Request to Camunda failed"}
        except Exception as e:
            logger.error("An unexpected error occurred: %s", e)
            return {"error": "Unexpected error"}

    async def check_for_updates(
        self,
        root: str,
        media_container_url: str,
        environment: str,
    ) -> bool:
        """
        Check for updates in DMN files and process them if necessary.
        """
        url = f"{media_container_url}/{root}/{environment}/data/camunda-dmn"
        current_etag = self.etags.get(environment)
        headers = {"If-None-Match": current_etag} if current_etag else {}

        logger.info(
            "Checking for updates in DMN files for environment: %s and headers: %s",
            environment,
            headers,
        )

        response = await self.client.get(url, headers=headers)

        if response.status_code == 304:
            logger.info(
                "Status 304. No changes detected in DMN files for environment: %s (ETag remains the same).",
                environment,
            )
            return False, current_etag

        new_etag = response.headers.get("ETag")
        tenant = new_etag

        if response.status_code != 200:
            logger.error(
                "Failed to fetch DMN files for environment: %s tenant: %s, status code: %s",
                environment,
                tenant,
                response.status_code,
            )
            return False, current_etag

        if self.etags.get(environment) and new_etag == self.etags.get(environment):
            logger.info(
                "No changes detected in DMN files for environment: %s tenant: %s (ETag remains the same).",
                environment,
                tenant,
            )
            return False, current_etag

        logger.info(
            "Etg has changed for environment: %s tenant: %s, processing DMN files. new etag: %s",
            environment,
            tenant,
            new_etag,
        )
        self.etags[environment] = new_etag

        await self._process_dmn_files(
            media_container_url, root, environment, tenant, response.json()
        )
        return True, new_etag

    async def _process_dmn_files(
        self,
        media_container_url: str,
        root: str,
        environment: str,
        tenant: str,
        files_data: list,
    ) -> None:
        """
        Process DMN files and write them to disk concurrently, using async file I/O.
        """
        # Filter for DMN files only
        dmn_files = [
            item["name"] for item in files_data if item["name"].endswith(".dmn")
        ]

        async def process_single_dmn_file(dmn_file_name: str):
            # Fetch file content
            file_content, _ = await self.fetch_dmn_file(
                media_container_url, root, environment, dmn_file_name
            )

            # Define file path and create directories if needed
            file_path = os.path.join(CAMUNDA_DATA_DIR, tenant, dmn_file_name)
            os.makedirs(
                os.path.dirname(file_path), exist_ok=True
            )  # Directory creation remains synchronous

            # Write file content to disk asynchronously
            async with aiofiles.open(file_path, "wb") as f:
                await f.write(file_content)

        # Create a list of tasks for each DMN file
        tasks = [process_single_dmn_file(dmn_file_name) for dmn_file_name in dmn_files]

        # Run all tasks concurrently
        await asyncio.gather(*tasks)

        logger.info("Done writing DMN files for tenant %s.", tenant)

    async def fetch_dmn_file(
        self,
        media_container_url: str,
        root: str,
        environment: str,
        dmn_file_name: str,
        max_retries: int = 10,
        sleep_interval: float = 5,
    ) -> Tuple[Optional[bytes], Optional[str]]:
        """
        Fetch a DMN file from the media container with retry logic.

        Args:
            media_container_url (str): Base URL of the media container.
            root (str): Root directory in the media container.
            environment (str): Environment name.
            dmn_file_name (str): Name of the DMN file to fetch.
            max_retries (int): Maximum number of retry attempts.
            sleep_interval (float): Fixed wait time between retries in seconds.

        Returns:
            Tuple[Optional[bytes], Optional[str]]: Content of the file and its ETag if successful, else (None, None).
        """
        # Ensure branch name is URL-encoded to handle special characters like '/'
        url = f"{media_container_url}/{root}/{environment}/data/camunda-dmn/{dmn_file_name}"

        for attempt in range(1, max_retries + 1):
            try:
                logger.debug("Attempt %d to fetch DMN file: %s", attempt, url)
                response = await self.client.get(url)

                if response.status_code == 302:
                    redirected_url = response.headers.get("location")
                    if not redirected_url:
                        logger.warning(
                            "Received 302 without 'Location' header for file: %s",
                            dmn_file_name,
                        )
                        raise ValueError(
                            "Redirected response missing 'Location' header."
                        )

                    logger.debug("Following redirect to: %s", redirected_url)
                    response = await self.client.get(redirected_url)

                if response.status_code in [200, 302]:
                    logger.debug("Successfully fetched DMN file: %s", dmn_file_name)
                    return response.content, response.headers.get("etag")
                else:
                    logger.warning(
                        "Failed to fetch DMN file %s. Status code: %s",
                        dmn_file_name,
                        response.status_code,
                    )

            except Exception as e:
                logger.error(
                    "Error fetching DMN file %s on attempt %d/%d: %s",
                    dmn_file_name,
                    attempt,
                    max_retries,
                    str(e),
                )

            if attempt < max_retries:
                logger.info(
                    "Retrying to fetch DMN file %s after %.2f seconds...",
                    dmn_file_name,
                    sleep_interval,
                )
                await asyncio.sleep(sleep_interval)
            else:
                logger.error(
                    "Exceeded maximum retries (%d) for DMN file: %s",
                    max_retries,
                    dmn_file_name,
                )

        return None, None

    async def delete_tenant(self, tenant: str):
        """
        Delete all deployments for a specific tenant.
        """
        url = f"{CAMUNDA_URL}/deployment"
        response = await self.client.delete(url, params={"tenantId": tenant})

        if response.status_code == 204:
            logger.info("Deleted tenant %s successfully.", tenant)
        else:
            logger.error(
                "Failed to delete tenant %s, status code: %s",
                tenant,
                response.status_code,
            )

    async def clear_cache(self, namespace: str = "camunda"):
        """
        Clear the cache for all stored Camunda definitions for a specific namespace.
        """
        await self.cache.clear(namespace=namespace)
        logger.info("Cache cleared for Camunda definitions in namespace %s.", namespace)

    async def get_dmn(self, filename: str, tenant: str):
        """
        Fetch and return DMN file content for the specified tenant asynchronously.
        """
        file_path = os.path.join(CAMUNDA_DATA_DIR, tenant, filename)
        async with aiofiles.open(file_path, "r") as file:
            return await file.read()

    async def add_dmn(self, dmn_dict: Dict[str, bytes], tenant: str):
        """
        Add DMN files to the specified tenant directory asynchronously.
        """
        tenant_dir = os.path.join(CAMUNDA_DATA_DIR, tenant)
        os.makedirs(tenant_dir, exist_ok=True)  # This can remain synchronous
        for filename, filedata in dmn_dict.items():
            async with aiofiles.open(os.path.join(tenant_dir, filename), "wb") as f:
                await f.write(filedata)
            logger.info("DMN file %s added for tenant %s.", filename, tenant)

    async def get_variables(
        self,
        definitions: Optional[Dict[str, Definition]] = None,
        tenant: str = "gem-media-container",
    ) -> Set[str]:
        """
        Get all input variables from the DMN files for the specified tenant.
        """
        variables = {}

        # Fetch definitions for the specified tenant
        definitions = definitions or await self.get_definitions(tenant)

        for d_key, d in definitions.items():
            # Construct the URL with tenant-id if necessary
            url = (
                f"{CAMUNDA_URL}/decision-definition/key/{d_key}/tenant-id/{tenant}/xml"
            )

            response = await self.client.get(url=url)  # Adding tenant-id as a parameter

            # Check for errors in response
            if response.status_code != 200:
                logger.error(
                    "Error fetching decision definition for key %s and tenant %s: %s",
                    d_key,
                    tenant,
                    response.text,
                )
                continue  # Skip this item if the request fails

            # Check if the response contains 'dmnXml'
            response_json = response.json()
            if "dmnXml" not in response_json:
                logger.error(
                    "dmnXml key missing in response for definition key %s. Response: %s",
                    d_key,
                    response_json,
                )
                continue  # Skip this item if dmnXml is missing

            # Parse the DMN XML
            try:
                dmn = ET.fromstring(response_json["dmnXml"])
            except ET.ParseError as e:
                logger.error(
                    "Error parsing DMN XML for definition key %s: %s", d_key, e
                )
                continue  # Skip this item if XML parsing fails

            # Extract variables
            for d in dmn.findall(
                "{https://www.omg.org/spec/DMN/20191111/MODEL/}decision"
            ):
                dt = d.find(
                    "{https://www.omg.org/spec/DMN/20191111/MODEL/}decisionTable"
                )
                if dt is not None:
                    for i in dt.findall(
                        "{https://www.omg.org/spec/DMN/20191111/MODEL/}input"
                    ):
                        input_variable = i.attrib.get(
                            "{http://camunda.org/schema/1.0/dmn}inputVariable"
                        )
                        if input_variable:
                            variables[input_variable] = {
                                "value": None,
                                "type": "String",
                            }

        return variables

    async def get_definitions(
        self, tenant: str = "gem-media-container"
    ) -> Dict[str, Definition]:
        """
        Fetch all decision definitions for the specified tenant.
        """
        url = str(CAMUNDA_URL / "decision-definition")
        # Pass tenantId as a parameter to filter definitions by tenant
        response = await self.client.get(
            url, params={"latestVersion": "true", "tenantId": tenant}
        )

        if response.status_code != 200:
            logger.error(
                "Failed to fetch definitions for tenant %s: %s", tenant, response.text
            )
            return {}

        definitions = {}
        for definition in response.json():
            definitions[definition["key"]] = Definition(
                version=definition["version"], name=definition["name"]
            )

        # Store definitions specifically for this tenant
        self.definitions[tenant] = definitions

        return definitions

    async def get_nlg_context(self, template, tracker):
        """
        get nlg context
        """
        url = os.getenv("NLG_URL")
        payload = json.dumps(
            {
                "response": template,
                "template": template,
                "arguments": {},
                "tracker": tracker,
                "channel": {"name": "socketio"},
            }
        )
        headers = {"Content-Type": "application/json"}

        async with httpx.AsyncClient() as client:
            response = await client.post(
                f"{url}?useCache=true", headers=headers, data=payload, timeout=5
            )
            response.raise_for_status()
            return response.json()

    async def update_project_name_to_tenant_mapping(
        self, project_name: str, tenant: str
    ):
        """
        Update the project name to tenant mapping in the cache.
        """
        try:
            await self.cache.set(project_name, tenant)
        except Exception as e:
            logger.error(
                "Failed to update mapping for %s -> %s: %s", project_name, tenant, e
            )

    async def get_project_name_from_tenant_mapping(self, project_name: str):
        """
        Get the tenant name from the project name.
        """
        try:
            tenant = await self.cache.get(project_name)
            if tenant:
                logger.info("Retrieved tenant: %s", tenant)
            else:
                logger.warning("No tenant found for project name: %s", project_name)
            return tenant
        except Exception as e:
            logger.error("Failed to retrieve tenant for %s: %s", project_name, e)
            return None

    async def get_tenant_mappings(self):
        """
        Get all project name to tenant mappings.
        """
        try:
            keys = await self.cache.keys("*")
            mappings = {key: await self.cache.get(key) for key in keys}
            logger.info("Retrieved all tenant mappings: %s", mappings)
            return mappings
        except Exception as e:
            logger.error("Failed to retrieve tenant mappings: %s", e)
            return {}

    async def camunda_reevaluate(self, payload, tenant: str = "gem-media-container"):
        """
        Reevaluate the DMN definitions for the given payload.
        """
        response_list, name_list, status_code, status_text = [], [], [], []

        base_municipality = None
        if "base_municipality" in payload.get("variables"):
            base_municipality = payload["variables"]["base_municipality"].get(
                "value", None
            )
            removed_key = payload["variables"].pop("base_municipality")

        for key in self.definitions.get(tenant, []):

            out = await self.evaluate_definition(
                definition_key=key, payload=payload, tenant=tenant
            )

            if not (out[0] == {} and out[1] is None):
                response_list.append(out[0])
                name_list.append(out[1])

            if out[2] and out[3]:
                status_code.append(out[2])
                status_text.append(out[3])

        return response_list, name_list, status_code, status_text

    async def evaluate(
        self, cms_base_url, cms_caching, payload, tenant: str = "gem-media-container"
    ):
        """
        Evaluate the DMN definitions for the given payload.
        """
        # Extract and remove base_municipality out of payload
        base_municipality = None
        if "base_municipality" in payload.get("variables"):
            base_municipality = payload["variables"]["base_municipality"].get(
                "value", None
            )
            payload["variables"].pop("base_municipality")

        # Extract and remove tracker out of payload
        tracker = payload.get("variables", {}).pop("tracker", None)

        # Extract and remove username out of payload
        username = payload["variables"].get("username", {}).get("value", None)
        if "username" in payload["variables"]:
            payload["variables"].pop("username")

        response_list, name_list, status_code, status_text = [], [], [], []

        for key in self.definitions.get(tenant, []):
            out = await self.evaluate_definition(
                definition_key=key, payload=payload, tenant=tenant
            )
            if not (out[0] == {} and out[1] is None):
                if isinstance(out[0], dict):
                    out[0]["source"] = {"value": out[1]}
                response_list.append(out[0])
                name_list.append(out[1])

            if out[2] and out[3]:
                status_code.append(out[2])
                status_text.append(out[3])

        if response_list:
            for response in response_list:
                if (
                    response.get("story_id", None)
                    and response["story_id"].get("value", None) != None
                ):
                    municipality = slugify(base_municipality)
                    story_id = response["story_id"]["value"]
                    data = {}

                    entry = cms_caching.get(municipality, [0, {}])
                    municipality_data = entry[1]
                    e_timestamp = entry[0]
                    c_timestamp = datetime.now().timestamp()

                    if c_timestamp - e_timestamp > 300 or username:
                        try:
                            logger.info(
                                "Retrieving municipality settings from %s", cms_base_url
                            )
                            response = await self.client.get(
                                f"{cms_base_url}/api/{municipality}/stories/", timeout=3
                            )
                            # response = requests.get(f"https://mijn.virtuele-gemeente-assistent.nl/api/{municipality}/stories/", timeout=3000)
                            response.raise_for_status()
                            municipality_data = response.json()
                            cms_caching[municipality] = [c_timestamp, municipality_data]
                        except Exception as e:
                            logger.exception(
                                "Something went wrong while fetching municipality settings. e: %s",
                                e,
                            )

                    data = next(
                        (
                            item
                            for item in municipality_data
                            if item["story_id"] == story_id
                        ),
                        {
                            "story_id": story_id,
                            "enabled": True,
                            "handover_livechat": False,
                            "crisis_answer": False,
                            "parking_answer": False,
                            "handover_org": "",
                        },
                    )

                    handover_org = None
                    if data.get("handover_to_organisation", None):
                        handover_org = data.get(
                            "handover_to_organisation", None
                        ).lower()

                    if handover_org:
                        # Contexual metadata extraction from original utter prediction
                        for response in response_list:
                            if response.get("response.1", None):
                                template = str(
                                    response["response.1"].get("value", None)
                                )
                                if template.startswith("utter"):
                                    try:
                                        nlg_response = await self.get_nlg_context(
                                            template=template, tracker=tracker
                                        )
                                        context_data = nlg_response.json()
                                    except Exception as e:
                                        logger.error(
                                            "Error fetching NLG context: %s", e
                                        )
                                        context_data = {}

                                    if context_data.get(
                                        "custom", None
                                    ) and context_data["custom"].get("context", None):
                                        utter_context = context_data["custom"].get(
                                            "context", None
                                        )
                                        for key in utter_context:
                                            payload["variables"]["context." + key] = {
                                                "value": utter_context[key],
                                                "type": "String",
                                            }
                                            payload["variables"]["context_" + key] = {
                                                "value": utter_context[key],
                                                "type": "String",
                                            }

                        response = await self.client.get(
                            f"{cms_base_url}/api/organisations/{handover_org.lower()}/",
                            timeout=3000,
                        )
                        response.raise_for_status()
                        data = response.json()

                        payload["variables"]["handover_organisation"] = {
                            "value": data.get("name", None),
                            "type": "String",
                        }
                        payload["variables"]["supports_handover"] = {
                            "value": str(data.get("supports_handover", None)),
                            "type": "String",
                        }
                        payload["variables"]["handover_logo"] = {
                            "value": data.get("municipality_logo", None),
                            "type": "String",
                        }
                        payload["variables"]["handover_url"] = {
                            "value": data.get("contact_url", None),
                            "type": "String",
                        }
                        payload["variables"]["intent"] = {
                            "value": "handover",
                            "type": "String",
                        }

                        (
                            response_list,
                            name_list,
                            status_code,
                            status_text,
                        ) = await self.camunda_reevaluate(
                            payload=payload, tenant=tenant
                        )
                        break

                    # If the story is disabled, reevalation takes place with enriched payload, so logic can be made within DMN.
                    elif not data["enabled"]:
                        payload["variables"]["intent"] = {
                            "value": "disabled",
                            "type": "String",
                        }

                        (
                            response_list,
                            name_list,
                            status_code,
                            status_text,
                        ) = await self.camunda_reevaluate(
                            payload=payload, tenant=tenant
                        )
                        break

                    # If the story is redirected to livechat, reevalation takes place with enriched payload, so logic can be made within DMN.
                    elif data["handover_livechat"]:
                        payload["variables"]["intent"] = {
                            "value": "medewerker_nodig",
                            "type": "String",
                        }

                        (
                            response_list,
                            name_list,
                            status_code,
                            status_text,
                        ) = await self.camunda_reevaluate(
                            payload=payload, tenant=tenant
                        )
                        break

                    # If the story is a crisis answer, reevalation takes place with enriched payload, so logic can be made within DMN.
                    elif data["crisis_answer"]:
                        payload["variables"]["intent"] = {
                            "value": "crisis",
                            "type": "String",
                        }

                        (
                            response_list,
                            name_list,
                            status_code,
                            status_text,
                        ) = await self.camunda_reevaluate(
                            payload=payload, tenant=tenant
                        )
                        break

                    # If the story is a parking answer, reevalation takes place with enriched payload, so logic can be made within DMN.
                    elif data["parking_answer"]:
                        payload["variables"]["intent"] = {
                            "value": "parkeren_algemeen",
                            "type": "String",
                        }

                        (
                            response_list,
                            name_list,
                            status_code,
                            status_text,
                        ) = await self.camunda_reevaluate(
                            payload=payload, tenant=tenant
                        )
                        break

        return response_list, name_list, status_code, status_text

    @cached(
        ttl=int(os.getenv("CACHE_EXPIRE", 60 * 60)),
        key_builder=_key_builder,
        serializer=JsonSerializer(),
        namespace=lambda *args, **kwargs: kwargs.get("tenant", "camunda"),  # Dynamic namespace per tenant
        cache=aiocache.RedisCache,
        endpoint="redis",
        port="6379",
    )
    async def evaluate_definition(
        self, definition_key: str, payload: Dict, tenant: str = "gem-media-container"
    ) -> Tuple[Dict, str]:
        """
        Evaluate a single DMN definition for the given payload.
        """
        logger.info("Evaluating definition: %s (Cache miss for key)", definition_key)

        url = f"{CAMUNDA_URL}/decision-definition/key/{definition_key}/tenant-id/{tenant}/evaluate"
        response = await self.client.post(url=url, json=payload)

        if response.status_code != 200:
            logger.error(
                "Response status code (%i): %s", response.status_code, response.text
            )
            return {}, None, response.status_code, response.text

        response_data = response.json()

        if not response_data:
            return {}, None, None, None

        return (
            response_data[0],
            self.definitions[tenant].get(definition_key, {}).name,
            None,
            None,
        )
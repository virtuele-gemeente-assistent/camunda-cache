# Description: Dockerfile for the FastAPI application
FROM python:3.9-slim-buster

RUN apt update && apt install -y --no-install-recommends git curl && rm -rf /var/lib/apt/lists/*

COPY requirements.txt requirements.txt
RUN pip install -r requirements.txt --no-cache-dir

WORKDIR /app

COPY ./camunda_cache camunda_cache
COPY server.py server.py

EXPOSE 8000

CMD ["uvicorn", "--port", "8000", "--host", "0.0.0.0", "server:app"]

# Add health check to verify the application is running
HEALTHCHECK --interval=15s --timeout=10s --start-period=10s --retries=10 \
    CMD curl -f http://localhost:8000/health || exit 1